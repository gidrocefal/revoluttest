package com.revolut.exchangerates.remote.ops;

import okhttp3.Request;

public class ExchangeRateApi {
    public Request getRates() {
        return new Request.Builder()
                .url("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")
                .build();
    }
}
