package com.revolut.exchangerates.remote.ops;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class ExchangeRateXmlParser {

    public HashMap<String, Float> parse(String string) throws XmlPullParserException, IOException {
        final InputStream is = new ByteArrayInputStream(string.getBytes());
        return parse(is);
    }

    private HashMap<String, Float> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);

            final XmlPullParser parser = factory.newPullParser();
            parser.setInput(in, null);
            parser.nextTag();

            return readRates(parser);
        } finally {
            in.close();
        }
    }

    private HashMap<String, Float> readRates(XmlPullParser parser) throws XmlPullParserException, IOException {
        final HashMap<String, Float> entries = new HashMap<>();

        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {

            final String tagName = parser.getName();
            final int attrsCount = parser.getAttributeCount();

            switch (eventType) {
                case XmlPullParser.END_TAG:
                    if (tagName.equals("Cube") && attrsCount == 2) {
                        final String code = parser.getAttributeValue(0);
                        final float rate = Float.parseFloat(parser.getAttributeValue(1));
                        entries.put(code, rate);
                    }
                    break;
            }

            eventType = parser.next();
        }

        return entries;
    }
}
