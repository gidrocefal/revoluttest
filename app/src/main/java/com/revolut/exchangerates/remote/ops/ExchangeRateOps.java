package com.revolut.exchangerates.remote.ops;

import com.revolut.exchangerates.model.ExchangeRateCollection;
import com.revolut.exchangerates.remote.RemoteRequestExecutor;

import org.threeten.bp.Instant;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class ExchangeRateOps {

    private final RemoteRequestExecutor mExecutor;
    private final ExchangeRateApi mApi;

    public ExchangeRateOps(RemoteRequestExecutor executor, ExchangeRateApi api) {
        mExecutor = executor;
        mApi = api;
    }

    public Observable<ExchangeRateCollection> requestRates() {
        return mExecutor.execute(mApi.getRates())
                .flatMap(READ_RESPONSE_AS_XML)
                .flatMap(PARSE_XML)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private static final Func1<Response, Observable<String>> READ_RESPONSE_AS_XML
            = new Func1<Response, Observable<String>>() {

        @Override
        public Observable<String> call(Response response) {
            try {
                final String resultString = response.body().string();
                return Observable.just(resultString);
            } catch (IOException e) {
                return Observable.error(e);
            }
        }
    };

    private static final Func1<String, Observable<ExchangeRateCollection>> PARSE_XML
            = new Func1<String, Observable<ExchangeRateCollection>>() {

        private final ExchangeRateXmlParser parser = new ExchangeRateXmlParser();

        @Override
        public Observable<ExchangeRateCollection> call(String s) {
            try {
                final HashMap<String, Float> map = parser.parse(s);
                final ExchangeRateCollection result = new ExchangeRateCollection(map, Instant.now());
                return Observable.just(result);
            } catch (XmlPullParserException | IOException e) {
                return Observable.error(e);
            }
        }
    };
}
