package com.revolut.exchangerates.remote;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.subscriptions.BooleanSubscription;

public class RemoteRequestExecutor {

    private static final OkHttpClient HTTP_CLIENT = new OkHttpClient.Builder()
            .build();

    public Observable<Response> execute(final Request request) {
        return Observable.create(new Observable.OnSubscribe<Response>() {
            @Override
            public void call(Subscriber<? super Response> subscriber) {

                final RemoteRequestHandler requestHandler = new RemoteRequestHandler(request);

                subscriber.add(BooleanSubscription.create(new Action0() {
                    @Override
                    public void call() {
                        requestHandler.cancel();
                    }
                }));

                try {
                    final Response response = requestHandler.execute();
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(response);
                        subscriber.onCompleted();
                    }
                } catch (IOException e) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(e);
                    }
                }
            }
        });
    }

    private class RemoteRequestHandler {

        private Request mRequest;
        private Call mCall;

        public RemoteRequestHandler(Request request) {
            mRequest = request;
        }

        public Response execute() throws IOException {
            mCall = HTTP_CLIENT.newCall(mRequest);
            return mCall.execute();
        }

        public void cancel() {
            if (mCall != null && !mCall.isCanceled()) {
                mCall.cancel();
            }
        }
    }
}
