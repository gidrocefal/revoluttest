package com.revolut.exchangerates.ui;

import com.revolut.exchangerates.model.ExchangeRateCollection;
import com.revolut.exchangerates.remote.ops.ExchangeRateOps;
import com.revolut.exchangerates.storage.ExchangeRateStorage;
import com.revolut.exchangerates.util.RxUtils;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;

public class ExchangeRateProvider {

    private ExchangeRateOps mExchangeRateOps;
    private ExchangeRateStorage mExchangeRateStorage;

    public ExchangeRateProvider(ExchangeRateOps exchangeRateOps, ExchangeRateStorage exchangeRateStorage) {
        mExchangeRateOps = exchangeRateOps;
        mExchangeRateStorage = exchangeRateStorage;
    }

    public Observable<ExchangeRateCollection> getExchangeRateObservable() {
        return mExchangeRateOps.requestRates()
                .retryWhen(RxUtils.retryAfterDelay(5, TimeUnit.SECONDS))
                .repeatWhen(RxUtils.repeatAfterDelay(30, TimeUnit.SECONDS))
                .doOnNext(new Action1<ExchangeRateCollection>() {
                    @Override
                    public void call(ExchangeRateCollection response) {
                        mExchangeRateStorage.setRates(response);
                    }
                })
                .startWith(mExchangeRateStorage.getRates());
    }
}
