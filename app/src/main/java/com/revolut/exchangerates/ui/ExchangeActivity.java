package com.revolut.exchangerates.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import com.revoult.exchangerates.R;
import com.revolut.exchangerates.model.Currency;
import com.revolut.exchangerates.model.CurrencyRate;
import com.revolut.exchangerates.model.ExchangeRateCollection;
import com.revolut.exchangerates.remote.RemoteRequestExecutor;
import com.revolut.exchangerates.remote.ops.ExchangeRateApi;
import com.revolut.exchangerates.remote.ops.ExchangeRateOps;
import com.revolut.exchangerates.storage.ExchangeRateStorage;
import com.revolut.exchangerates.storage.SavingsStorage;
import com.revolut.exchangerates.util.DateTimeFormatUtils;
import com.revolut.exchangerates.view.ExchangeView;

import org.threeten.bp.Instant;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;


public class ExchangeActivity extends AppCompatActivity {

    private final SavingsStorage mSavingsStorage = new SavingsStorage();

    private ExchangeRateProvider mRatesProvider;
    private CompositeSubscription mSubscription;

    private ExchangeRateCollection mLatestRateUpdate;

    private TextView mUpdatedView;

    private Currency mCurrencyFrom = Currency.EUR;
    private Currency mCurrencyTo = Currency.USD;

    private ExchangeView mCurrencyFromView;
    private ExchangeView mCurrencyToView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);

        final RemoteRequestExecutor executor = new RemoteRequestExecutor();
        final ExchangeRateApi api = new ExchangeRateApi();
        final ExchangeRateOps exchangeRateOps = new ExchangeRateOps(executor, api);
        final ExchangeRateStorage exchangeRateStorage = new ExchangeRateStorage(this);

        mRatesProvider = new ExchangeRateProvider(exchangeRateOps, exchangeRateStorage);

        mUpdatedView = (TextView) findViewById(R.id.exchange_updated);

        mCurrencyFromView = (ExchangeView) findViewById(R.id.exchange_from);
        mCurrencyToView = (ExchangeView) findViewById(R.id.exchange_to);

        updateUI();
    }

    private void updateUI() {
        mCurrencyFromView.setSelectedCurrency(mCurrencyFrom);
        mCurrencyFromView.setTitle(formatTitleString(mCurrencyFrom));
        mCurrencyFromView.setTitleDescription(formatSavingsString(mCurrencyFrom));

        mCurrencyToView.setSelectedCurrency(mCurrencyTo);
        mCurrencyToView.setTitle(formatTitleString(mCurrencyTo));
        mCurrencyToView.setTitleDescription(formatSavingsString(mCurrencyTo));

        if (mLatestRateUpdate != null) {
            mUpdatedView.setText(formatUpdatedString(mLatestRateUpdate.updated));

            final CurrencyRate fromRate = getCurrencyRate(mCurrencyFrom);
            final CurrencyRate toRate = getCurrencyRate(mCurrencyTo);

            mCurrencyToView.setValueDescription(formatRateString(fromRate, toRate));

            if (mCurrencyToView.isInFocus()) {
                float value = mCurrencyToView.getInputValue() * (fromRate.rate / toRate.rate);
                mCurrencyFromView.setInputValue(value);
            } else {
                float value = mCurrencyFromView.getInputValue() * (toRate.rate / fromRate.rate);
                mCurrencyToView.setInputValue(value);
            }
        } else {
            mUpdatedView.setText("Waiting for exchange rates…");
        }
    }

    private String formatUpdatedString(Instant instant) {
        return getString(R.string.exchange_updated,
                DateTimeFormatUtils.formatSimpleLocalDateTime(instant));
    }

    private String formatTitleString(Currency currency) {
        return currency.code.toUpperCase();
    }

    private String formatSavingsString(Currency currency) {
        return getString(R.string.exchange_available_amount, currency.symbol +
                String.valueOf(mSavingsStorage.getSavingsByCode(currency.code)));
    }

    private String formatRateString(CurrencyRate fromRate, CurrencyRate toRate) {
        final float rate = fromRate.rate / toRate.rate;
        return formatAmount(toRate, 1.0f) + " = " + formatAmount(fromRate, rate);
    }

    private String formatAmount(CurrencyRate currencyRate, float amount) {
        return currencyRate.currency.symbol + String.valueOf(amount);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSubscription = new CompositeSubscription();
        mSubscription.add(mRatesProvider.getExchangeRateObservable()
                .subscribe(new Action1<ExchangeRateCollection>() {
                    @Override
                    public void call(ExchangeRateCollection response) {
                        mLatestRateUpdate = response;
                        updateUI();
                    }
                }));

        mCurrencyFromView.setListener(new ExchangeView.Listener() {
            @Override
            public void onCurrencySelected(Currency currency) {
                mCurrencyFrom = currency;
                updateUI();
            }

            @Override
            public void onValueChanged(float value) {
                updateUI();
            }
        });

        mCurrencyToView.setListener(new ExchangeView.Listener() {
            @Override
            public void onCurrencySelected(Currency currency) {
                mCurrencyTo = currency;
                updateUI();
            }

            @Override
            public void onValueChanged(float value) {
                updateUI();
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    protected void onPause() {
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
        super.onPause();
    }

    private CurrencyRate getCurrencyRate(Currency currency) {
        if (Currency.CODE_EUR.equals(currency.code)) {
            return new CurrencyRate(Currency.EUR, 1.0f);
        }

        return new CurrencyRate(currency, mLatestRateUpdate.rates.get(currency.code));
    }
}
