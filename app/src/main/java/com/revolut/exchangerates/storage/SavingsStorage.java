package com.revolut.exchangerates.storage;

import com.revolut.exchangerates.model.Currency;

import java.util.HashMap;

public class SavingsStorage {

    private HashMap<String, Float> mSavings = new HashMap<>();

    public SavingsStorage() {
        // Add some for test purpose
        mSavings.put(Currency.CODE_EUR, 104.23f);
        mSavings.put(Currency.CODE_USD, 991.78f);
        mSavings.put(Currency.CODE_GBP, 12.67f);
    }

    public float getSavingsByCode(String currencyCode) {
        if (mSavings.containsKey(currencyCode)) {
            return mSavings.get(currencyCode);
        }

        return 0.0f;
    }
}
