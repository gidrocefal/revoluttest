package com.revolut.exchangerates.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.revolut.exchangerates.model.Currency;
import com.revolut.exchangerates.model.ExchangeRateCollection;

import org.threeten.bp.Instant;

import java.util.HashMap;

public class ExchangeRateStorage {

    private static final String KEY_UPDATED = "key.saved";

    private SharedPreferences mSharedPreferences;
    private ExchangeRateCollection mCache;

    // Storage for only rates we are interested in
    public ExchangeRateStorage(Context context) {
        mSharedPreferences = context.getSharedPreferences("exchange_rate_storage", Context.MODE_PRIVATE);
    }

    public void setRates(ExchangeRateCollection collection) {
        saveRates(collection);
        mCache = collection;
    }

    public ExchangeRateCollection getRates() {
        return mCache != null ? mCache : loadRates();
    }

    private void saveRates(ExchangeRateCollection collection) {
        mSharedPreferences.edit()
                .putFloat(Currency.USD.code, collection.rates.get(Currency.USD.code))
                .putFloat(Currency.GBP.code, collection.rates.get(Currency.GBP.code))
                .putString(KEY_UPDATED, collection.updated.toString())
                .apply();
    }

    private ExchangeRateCollection loadRates() {
        if (mSharedPreferences.contains(KEY_UPDATED)) {

            final Instant time = Instant.parse(mSharedPreferences.getString(KEY_UPDATED, ""));

            final HashMap<String, Float> map = new HashMap<>();
            map.put(Currency.USD.code, getRateByCode(Currency.USD.code));
            map.put(Currency.GBP.code, getRateByCode(Currency.GBP.code));

            return new ExchangeRateCollection(map, time);
        }

        return null;
    }

    private float getRateByCode(String code) {
        return mSharedPreferences.getFloat(code, 0.0f);
    }
}
