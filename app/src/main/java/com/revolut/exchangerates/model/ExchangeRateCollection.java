package com.revolut.exchangerates.model;

import org.threeten.bp.Instant;

import java.util.HashMap;

public class ExchangeRateCollection {

    public final HashMap<String, Float> rates;
    public final Instant updated;

    public ExchangeRateCollection(HashMap<String, Float> rates, Instant updated) {
        this.rates = rates;
        this.updated = updated;
    }
}
