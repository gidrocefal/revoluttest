package com.revolut.exchangerates.model;

public class Currency {

    public static final String CODE_EUR = "EUR";
    public static final String CODE_USD = "USD";
    public static final String CODE_GBP = "GBP";

    public static final Currency EUR = new Currency(CODE_EUR, "€");
    public static final Currency USD = new Currency(CODE_USD, "$");
    public static final Currency GBP = new Currency(CODE_GBP, "£");

    public final String code;
    public final String symbol;

    public Currency(String code, String symbol) {
        this.code = code;
        this.symbol = symbol;
    }
}
