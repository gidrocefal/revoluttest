package com.revolut.exchangerates.model;

public class CurrencyRate {

    public final Currency currency;
    public final float rate;

    public CurrencyRate(Currency currency, float rate) {
        this.currency = currency;
        this.rate = rate;
    }
}
