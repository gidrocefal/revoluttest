package com.revolut.exchangerates.util;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;

public class RxUtils {
    public static Func1<Observable<?>, Observable<?>> repeatAfterDelay(
            final long delay, final TimeUnit unit
    ) {
        return new Func1<Observable<?>, Observable<?>>() {
            @Override
            public Observable<?> call(Observable<?> observable) {
                return observable.delay(delay, unit);
            }
        };
    }

    public static Func1<Observable<? extends Throwable>, Observable<?>> retryAfterDelay(
            final long delay, final TimeUnit unit
    ) {
        return new Func1<Observable<? extends Throwable>, Observable<?>>() {
            @Override
            public Observable<?> call(Observable<? extends Throwable> observable) {
                return observable.flatMap(new Func1<Throwable, Observable<?>>() {
                    @Override
                    public Observable<?> call(Throwable throwable) {
                        return Observable.timer(delay, unit);
                    }
                });
            }
        };
    }

    public static <T> Func1<T, Boolean> filterNonNull() {
        return new Func1<T, Boolean>() {
            @Override
            public Boolean call(T object) {
                return object != null;
            }
        };
    }
}
