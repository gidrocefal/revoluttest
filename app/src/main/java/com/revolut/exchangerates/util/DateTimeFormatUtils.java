package com.revolut.exchangerates.util;

import org.threeten.bp.Instant;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.TimeZone;

public class DateTimeFormatUtils {

    private static DateTimeFormatter LOCAL_DATE_TIME = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    public static String formatSimpleLocalDateTime(Instant instant) {
        final OffsetDateTime dateTime = toLocalDateTime(instant);
        return dateTime.format(LOCAL_DATE_TIME);
    }

    private static OffsetDateTime toLocalDateTime(Instant instant) {
        final int rawOffsetInSec = TimeZone.getDefault().getRawOffset() / 1000;
        final ZoneOffset zoneOffset = ZoneOffset.ofTotalSeconds(rawOffsetInSec);
        return instant.atOffset(zoneOffset);
    }
}
