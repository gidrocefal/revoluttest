package com.revolut.exchangerates.util;

public class Utils {
    public static float parseFloatSafely(String s) {
        float result = 0.0f;

        try {
            result = Float.parseFloat(s);
        } catch (NumberFormatException ignored) {
        }

        return result;
    }

    public static int indexOfChar(char lookingFor, CharSequence charSequence) {
        final int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            final char c = charSequence.charAt(i);
            if (c == lookingFor) {
                return i;
            }
        }

        return -1;
    }
}
