package com.revolut.exchangerates.view;

import android.content.Context;
import android.support.v4.widget.TextViewCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.revoult.exchangerates.R;
import com.revolut.exchangerates.model.Currency;
import com.revolut.exchangerates.util.Utils;

public class ExchangeView extends FrameLayout {

    public interface Listener {
        void onCurrencySelected(Currency currency);
        void onValueChanged(float value);
    }

    private Listener mListener;

    private TextView mEurView;
    private TextView mUsdView;
    private TextView mGbpView;

    private TextView mSelectedCurrencyView;

    protected TextView mTitleView;
    protected TextView mTitleDescriptionView;
    protected EditText mValueView;
    protected TextView mValueDescriptionView;

    public ExchangeView(Context context) {
        super(context);
        init();
    }

    public ExchangeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExchangeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ExchangeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        View.inflate(getContext(), R.layout.widget_exchange_view, this);

        mEurView = (TextView) findViewById(R.id.exchange_switcher_eur);
        mUsdView = (TextView) findViewById(R.id.exchange_switcher_usd);
        mGbpView = (TextView) findViewById(R.id.exchange_switcher_gbp);

        mTitleView = (TextView) findViewById(R.id.exchange_title);
        mTitleDescriptionView = (TextView) findViewById(R.id.exchange_available_amount);
        mValueDescriptionView = (TextView) findViewById(R.id.exchange_description);
        mValueView = (EditText) findViewById(R.id.exchange_value);

        mValueView.addTextChangedListener(mValueTextWatcher);
        mValueView.setOnFocusChangeListener(mFocusChangedListener);

        mEurView.setOnClickListener(mSwitcherClickListener);
        mUsdView.setOnClickListener(mSwitcherClickListener);
        mGbpView.setOnClickListener(mSwitcherClickListener);

        if (isInEditMode()) {
            fillWithDummyData();
        }
    }

    private final OnFocusChangeListener mFocusChangedListener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            mValueView.setHint(b ? "" : getContext().getString(R.string.exchange_input_hint));

            if (mListener != null) {
                final float currentValue = Utils.parseFloatSafely(mValueView.getText().toString());
                mListener.onValueChanged(currentValue);
            }
        }
    };

    private final OnClickListener mSwitcherClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            Currency selectedCurrency = null;

            switch (view.getId()) {
                case R.id.exchange_switcher_eur:
                    selectedCurrency = Currency.EUR;
                    break;
                case R.id.exchange_switcher_usd:
                    selectedCurrency = Currency.USD;
                    break;
                case R.id.exchange_switcher_gbp:
                    selectedCurrency = Currency.GBP;
                    break;
            }

            if (mListener != null) {
                mListener.onCurrencySelected(selectedCurrency);
            }
        }
    };

    private void highlightSelectedCurrencyView(TextView currencyView) {
        if (mSelectedCurrencyView != null) {
            TextViewCompat.setTextAppearance(mSelectedCurrencyView, R.style.CurrencyButton_Default);
        }

        mSelectedCurrencyView = currencyView;
        TextViewCompat.setTextAppearance(mSelectedCurrencyView, R.style.CurrencyButton_Selected);
    }

    private final TextWatcher mValueTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {

            final String currentInput = editable.toString();
            final float currentValue = Utils.parseFloatSafely(currentInput);

            final int length = editable.length();
            final int dotPosition = Utils.indexOfChar('.', editable);

            if (dotPosition != -1 && length > dotPosition + 3) {
                editable.delete(dotPosition + 3, length);
            }

            clearSpans(editable);

            if (mValueView.isFocused()) {
                if (mListener != null) {
                    mListener.onValueChanged(currentValue);
                }
            }
        }
    };

    private void clearSpans(Editable editable){
        final CharacterStyle[] toBeRemovedSpans = editable.getSpans(0, editable.length(),
                MetricAffectingSpan.class);
        for (int i = 0; i < toBeRemovedSpans.length; i++) {
            editable.removeSpan(toBeRemovedSpans[i]);
        }
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public float getInputValue() {
        return Utils.parseFloatSafely(mValueView.getText().toString());
    }

    public boolean isInFocus() {
        return mValueView.isFocused();
    }

    public void setSelectedCurrency(Currency currency) {
        switch (currency.code) {
            case Currency.CODE_EUR:
                highlightSelectedCurrencyView(mEurView);
                break;
            case Currency.CODE_USD:
                highlightSelectedCurrencyView(mUsdView);
                break;
            case Currency.CODE_GBP:
                highlightSelectedCurrencyView(mGbpView);
                break;
        }
    }

    public void setTitle(String title) {
        mTitleView.setText(title);
    }

    public void setTitleDescription(String titleDescription) {
        mTitleDescriptionView.setText(titleDescription);
    }

    public void setInputValue(float value) {
        String valueString = value > 0 ? String.valueOf(value) : "";
        mValueView.setText(valueString);
    }

    public void setValueDescription(String valueDescription) {
        mValueDescriptionView.setText(valueDescription);
    }

    private void fillWithDummyData() {
        mTitleView.setText("EUR");
        mTitleDescriptionView.setText("You have €100");
        mValueDescriptionView.setText("€1 = $0.92");
        mValueView.setText("100.0");
    }
}
